package ms.src.code.Gui;

/* 프레임 생성 하는 방법 1 번째 JFrame 상속 */

import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.JFrame;

//								 JFrame은 Frame 상속
public class JFrame01 extends JFrame{
	
	public JFrame01() {	
		JFrame jf = new JFrame();
	
		// 프레임 크기 설정
//		this.setLocation(300,200);
		// 프레임 위치(사이즈) 설정
//		this.setSize(800,500);
		
		// 크기 + 위치 를 한번에 지정할수 있다.
		this.setBounds(300,200,800,500);
		// 프레임 상단에 이름 설정
		this.setTitle("MS_CodeFrame");
		// 상단 로고 try + catch 로 감싸준다. 
		try {
			this.setIconImage(ImageIO.read(new File("images/icon.png")));
		} catch (IOException e) {
			
			e.printStackTrace();
		}
		
		// 기본값 true, false로 주면 창 크기 변화 불가 (고정값)
		this.setResizable(false);
		
		
		// 1.크기&위치 정했으면 프레임을 보여주게하는 메소드
		this.setVisible(true);
		// 닫기 버튼을 누르면 프로세스 같이 종료 되는 메소드.
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
	}
	
	public static void main(String[] args) {
		
		new JFrame01();
	}
	
}