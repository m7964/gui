package ms.src.code.Gui;

import javax.imageio.ImageIO;
import javax.swing.JFrame;
import java.awt.Rectangle;
import java.io.File;
import java.io.IOException;

public class JFrame02 {
	
	/* 프레임 생성 하는 방법 2 번째 
	 * javax.swing.JFrame 클래스를 인스턴스로 생성후 해당 프레임 인스턴스의 설정값을 변경.
	 * 
	 * */
	
	public void showMainFrame() {
		
		JFrame mainFrame = new JFrame();
		mainFrame.setTitle("MS_Code");		//생성된 객체에 프레임 이름 설정
		
//		mainFrame.setBounds(300,200,800,500);
		/* Rectangle(뜻 : 사각형) 객체를 활용한 사이즈 설정 */
		Rectangle rectAngle = new Rectangle(300,200,800,500);	//import java.awt.Rectangle; 안되면 임폴트
		mainFrame.setBounds(rectAngle);	//기존 크기&위치값 설정 메소드에 Rectangle 참조변수를 넣었다.
		
		try {
			mainFrame.setIconImage(ImageIO.read(new File("images/icon.png")));
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
		mainFrame.setVisible(true);			// 크기& 위치값 설정후 보이게 하는 메소드 setVisible(true)
		mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

	}
	
	public static void main(String[] args) {
		
		new JFrame02().showMainFrame();
		
		
	}
	
	
	

}